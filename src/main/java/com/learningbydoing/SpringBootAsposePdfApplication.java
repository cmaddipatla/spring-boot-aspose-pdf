package com.learningbydoing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootAsposePdfApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootAsposePdfApplication.class, args);
	}
}
