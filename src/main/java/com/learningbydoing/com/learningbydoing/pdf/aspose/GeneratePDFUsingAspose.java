package com.learningbydoing.com.learningbydoing.pdf.aspose;

import com.aspose.pdf.Document;
import com.aspose.pdf.HtmlLoadOptions;
import com.aspose.pdf.License;
import com.aspose.pdf.PageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.ByteArrayOutputStream;
import java.net.URL;
import java.net.URLConnection;

@RestController
public class GeneratePDFUsingAspose {
    private static final Logger logger = LoggerFactory.getLogger(GeneratePDFUsingAspose.class);

    @Value("${web.page.url}")
    private String webpageURL;

    static {
        try {
            new License().setLicense(GeneratePDFUsingAspose.class.getClassLoader().getResourceAsStream("Aspose.Pdf.lic"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/getpdf", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getPDF1() throws Exception {
        logger.info("Got request...");
        ResponseEntity<byte[]> response = new ResponseEntity<byte[]>(HttpStatus.OK);
        HttpHeaders headers = new HttpHeaders();

        headers.setContentType(MediaType.parseMediaType("application/pdf"));
        String filename = "asposepdf.pdf";
        headers.add("content-disposition", "inline;filename=" + filename);
        //if we set this, pdf will be downloaded automatically by browser instead of displaying in browser.
//        headers.setContentDispositionFormData(filename, filename);
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        StringBuilder sb = new StringBuilder();
        try {
            URL url = new URL(webpageURL);
            logger.info("Trying to open URLConnection");
            URLConnection urlConnection = url.openConnection();
            logger.info("URLConnection opened");
            HtmlLoadOptions options = new HtmlLoadOptions(webpageURL);
            PageInfo info = new PageInfo();
            info.setLandscape(true);
            options.setPageInfo(info);
            Document document = new Document(urlConnection.getInputStream(), options);
            logger.info("Aspose Document created...");
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            document.save(outputStream);
            logger.info("Response written to outputstream...");
            return new ResponseEntity<byte[]>(outputStream.toByteArray(), headers, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }
}
